package vn.com.football.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

public class RequestUtil {

    private static CloseableHttpClient client = HttpClientBuilder.create().build();
    private final static String USER_AGENT = "Mozilla/5.0";

    public static void main(String... strings) throws ClientProtocolException, IOException {
        httpGet();
    }

    public static void httpGet() throws IOException, ClientProtocolException {
        String url = "http://www.google.com/search?q=Soái+ca";
        HttpGet request = new HttpGet(url);
        request.addHeader("User-Agent", USER_AGENT);
        CloseableHttpResponse response = client.execute(request);
        System.out.println(response.getStatusLine().getStatusCode());
        System.out.println(EntityUtils.toString(response.getEntity()));
    }

    public static void httpPost() throws ClientProtocolException, IOException {
        String url = "https://selfsolve.apple.com/wcResults.do";
        HttpPost post = new HttpPost(url);
        post.setHeader("User-Agent", USER_AGENT);
        List<NameValuePair> urlParameters = new ArrayList<>();
        urlParameters.add(new BasicNameValuePair("sn", "C02G8416DRJM"));
        urlParameters.add(new BasicNameValuePair("cn", ""));
        urlParameters.add(new BasicNameValuePair("locale", ""));
        urlParameters.add(new BasicNameValuePair("caller", ""));
        urlParameters.add(new BasicNameValuePair("num", "12345"));
        post.setEntity(new UrlEncodedFormEntity(urlParameters));
        CloseableHttpResponse response = client.execute(post);
        System.out.println(response.getStatusLine().getStatusCode());
    }

}
